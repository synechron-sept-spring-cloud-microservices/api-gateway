package com.synechron.apigateway.filter;

public class UserContextHolder {

    private static ThreadLocal<UserContext> userContextHolder;

    public static UserContext getUserContext(){
        if (userContextHolder.get() == null){
            userContextHolder.set(new UserContext());
        }
        return userContextHolder.get();
    }

    public static void setCorrelationId(String correlationId){
        userContextHolder.get().setCorrelationId(correlationId);
    }
    public  static void setAuthToken(String authToken){
        userContextHolder.get().setAuthToken(authToken);
    }
}