package com.synechron.apigateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Component
@Slf4j
public class TrackingFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("Injecting the correlation id ::");
        final List<String> correlationIds = exchange.getRequest().getHeaders().get("CORRELATION_ID");
        if(Objects.isNull(correlationIds) || correlationIds.isEmpty()){
            final String uuid = UUID.randomUUID().toString();
            exchange.getRequest().mutate().header("CORRELATION_ID", uuid);
            log.info("Injecting the UUID in the request header {} ::  "+ uuid);
        }
        return chain.filter(exchange);
    }
}